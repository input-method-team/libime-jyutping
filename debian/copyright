Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libime-jyutping
Upstream-Contact: CSSlayer <wengxt@gmail.com>
Source: https://download.fcitx-im.org/fcitx5/libime-jyutping/

Files: *
Copyright:
 2017-2022 CSSlayer <wengxt@gmail.com>
License: LGPL-2.1-or-later

Files: debian/*
Copyright: 2022 Boyuan Yang <byang@debian.org>
License: CC0-1.0

Files:
 data/jyutping*
Copyright:
 2017-2022 CSSlayer <wengxt@gmail.com>
 2006-2015 Peng Wu <alexepico@gmail.com>
 2002-2006 James Su <suzhe@tsinghua.edu.cn>
 Gong Chen <chen.sst@gmail.com>
 雪齋 <leoyoontsaw@gmail.com>
License: GPL-3.0-or-later
Comment:
 The data file is derived from libpinyin and rime-jyutping,
 and it is released under GPLv3+.

Files:
 src/engine/org.fcitx.Fcitx5.Addon.Jyutping.metainfo.xml.in
Copyright:
 2017-2022 CSSlayer <wengxt@gmail.com>
License: CC0-1.0

License: CC0-1.0
 On Debian systems, the full text of the CC0 1.0 Universal License
 can be found in the file `/usr/share/common-licenses/CC0-1.0'.

License: GPL-3.0-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of GPL-3 could be found at
 `/usr/share/common-licenses/GPL-3'.

License: LGPL-2.1-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of LGPL-2.1 could be found at
 `/usr/share/common-licenses/LGPL-2.1'.
