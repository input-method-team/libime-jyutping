Source: libime-jyutping
Section: utils
Priority: optional
Maintainer: Debian Input Method Team <debian-input-method@lists.debian.org>
Uploaders:
 Boyuan Yang <byang@debian.org>,
Build-Depends:
 cmake (>= 3.7),
 debhelper-compat (= 13),
 doxygen <!nodoc>,
 extra-cmake-modules,
 fcitx5-module-punctuation-dev,
 fcitx5-modules-dev,
 gettext,
 libboost-dev,
 libboost-iostreams-dev,
 libfcitx5core-dev (>= 5.1.12),
 libfcitx5utils-dev,
 libfmt-dev,
 libime-bin,
 libimecore-dev (>= 1.0.16~),
 libzstd-dev,
 pkg-config,
Rules-Requires-Root: no
Standards-Version: 4.6.1
Homepage: https://github.com/fcitx/libime-jyutping
Vcs-Git: https://salsa.debian.org/input-method-team/libime-jyutping.git
Vcs-Browser: https://salsa.debian.org/input-method-team/libime-jyutping

Package: libime-data-jyutping
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 libime-data-jyutping-language-model (>= ${source:Version}),
Description: Fcitx5 libime implementation of jyutping input method (data)
 This software provides a library that makes use of libime to implement
 jyutping (粵拼) input method. It also includes an engine for fcitx 5.
 .
 This package provides the architecture-independent dictionary files.
 
Package: libime-data-jyutping-language-model
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
Description: Fcitx5 libime implementation of jyutping input method (model)
 This software provides a library that makes use of libime to implement
 jyutping (粵拼) input method. It also includes an engine for fcitx 5.
 .
 This package provides the architecture-dependent langauge model files.

Package: libimejyutping-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 libimejyutping1 (= ${binary:Version}),
Description: Fcitx5 libime implementation of jyutping input method (dev)
 This software provides a library that makes use of libime to implement
 jyutping (粵拼) input method. It also includes an engine for fcitx 5.
 .
 This package provides the library development files.

Package: libimejyutping1
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 libime-data-jyutping (= ${source:Version}),
Description: Fcitx5 libime implementation of jyutping input method (lib)
 This software provides a library that makes use of libime to implement
 jyutping (粵拼) input method. It also includes an engine for fcitx 5.
 .
 This package provides the shared library files.

Package: fcitx5-jyutping
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Fcitx5 libime implementation of jyutping input method
 This software provides a library that makes use of libime to implement
 jyutping (粵拼) input method. It also includes an engine for fcitx 5.
 .
 This package provides the jyutping input method.

Package: libime-jyutping-bin
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Fcitx5 libime implementation of jyutping input method (tools)
 This software provides a library that makes use of libime to implement
 jyutping (粵拼) input method. It also includes an engine for fcitx 5.
 .
 This package provides the libime_jyutpingdict tool to handle jyutping
 dictionary.
